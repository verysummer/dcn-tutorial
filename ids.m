function [ID] = ids(who,varargin)
% BEBO IDs
% Input:
% - who (STRING): ya, ch, all 
% Output:
% - ID (DOUBLE)

if nargin == 1
    YA = [3109,3110,3111,3112,3113,3114,3115,3117,3118,3119,3120,3121,3122,3123,3124,3125,3126,3127,3128,3129,3130,3131,3132, ...
         3205,3206,3207,3208,3209,3211,3212,3213,3214,3215,3216,3217,3218,3219,3220,3222];

    CH = [2101, 2102, 2201, 2103, 2202, 2203, 2104, 2204, 2205, 2105, 2106, 2107, 2206, 2108, 2109, 2110, 2111, 2112, 2113, 2114, 2115];

elseif nargin > 1 && strcmp(varargin{1},'tutorial')
    YA = [3113,3115,3119,3122,3132,3208,3213,3214,3218,3219];
     
    CH = [2102,2104,2110,2112,2113,2114,2201,2203,2205,2206];
    % CH = {'02CSS25', '02DSW27', '03JBJ25',  '04CJC10',  '06KRB20', '07KNJ16',  '09KRE29',  '10SSC13', '11SWC30', '12SLZ15'}; % old names
end

% which IDs
if strcmp(who,'all') % all IDs for final analyses        
    ID = [CH, YA];
elseif strcmp(who,'ch') || strcmp(who,'CH') 
    ID = CH;
elseif strcmp(who,'ya') || strcmp(who,'YA')
    ID = YA;
end
    
    
end

    
