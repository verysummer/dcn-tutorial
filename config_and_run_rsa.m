%% Set configurations (config structure) and run RSA functions step by step
%
% VRS April 2021 
%
% Specify your configurations in config structure below with the following options:
%
% config.pdat.code  = string, path to folder in which this script is located
% config.pdat.ft    = string, path to fieldtrip folder (download from https://www.fieldtriptoolbox.org/download/)
% config.pdat.tfr   = string, path to input (EEG) data folder (download from OSF https://osf.io/jbrsa/)
% config.pdat.rsa   = string, path to output data folder (single-subject similarity data)
% config.pdat.ga    = string, path to output data folder (grand average similarity data)
% config.pdat.fig   = string, path to output figure folder 
% config.pdat.stat  = string, path to output stats folder
% config.pdat.beh   = string, path to input (behavioral) data (download from OSF https://osf.io/jbrsa/)
%
% config.group      = string, can be 'CH' (children group), 'YA' (young adults group)
% config.subjects   = int or 1xN double with selection of ID(s) (e.g. 2101 or [2101, 2102, 2103]). Children IDs start with 2, adult IDs start with 3. 
% config.type       = string, can be 'within-item' (correlates patterns of first and second presentation of an item (first exemplars)),
%                                    'within-cat' (correlates patterns of first and second exemplar of a category (first presentations)),
%                                    'between-cat' (correlates each item with all other items (first presentation of first exemplar)) 
%
% Optional:
% config.save       = logical, can be true (1) to save results (data/figures) or false (0) to only run without saving (default = 1)

restoredefaultpath;
clear variables; close all; clc;
disp('setting up paths...')

%% *** Set your configurations here *** 

config = [];

% Path configurations
config.pdat.code = '\\home\Tutorial\Scripts\tf-rsa-tutorial\'; % Tutorial scripts repo
addpath(genpath(config.pdat.code))

config.pdat.ft = '\\home\fieldtrip-20180709\'; % Your fieldtrip version 
addpath(config.pdat.ft); ft_defaults

config.pdat.tfr = '\\home\Tutorial\EEG_Data\'; % input data (time-frequency representations)
config.pdat.rsa = '\\home\Tutorial\Similarity_Data\'; % where to save output data (similarity matrices)
config.pdat.ga = '\\home\Tutorial\GA\'; % where to save output group grand average data (similarity matrices)
config.pdat.fig = '\\home\Tutorial\Figures\'; % where to save figures (similarity matrices, clusters etc)
config.pdat.stat = '\\home\Tutorial\Stats\'; % where to save statistics
config.pdat.beh = '\\home\Tutorial\'; % where is the behavioral data (CH+YA_mean_item_memory.mat)

% RSA configurations
config.group = 'YA'; % 'CH' = children, 'YA' = young adults
config.subjects = ids(config.group, 'tutorial'); % uses all subject IDs of the specified group
%config.subjects = [2206]; % chose your own subjects
config.type = 'within-item'; % within-item, within-cat, between-cat
%config.save = false; % save results/figures? (default = true)


%% Step 1: Run RSA for each subject
% required config fields: subjects, type, pdat.tfr, pdat.rsa

step1_rsa_get_sim_matrices(config)


%% Step 2: Compute similarity grand average
% required config fields: group, subjects, type, pdat.rsa, pdat.ga

step2_rsa_group_ga(config)


%% Step 3: Plot average similarity matrix
% required config fields: group, type*, pdat.ga, pdat.fig (* if type is not specified, plots within-item & within-cat)

step3_plot_sim_matrices(config)


%% Step 4: Test for item specificity (within-item versus within-category similarity)
% required config fields: subjects, group, pdat.tfr, pdat.rsa, pdat.stat, pdat.ga

% 1. Test within-item similarity against between-item (within-category) similarity for each subject (first level)

stat = step4a_sim_comparison_1st_level(config);

% 2. Test first-level t-values (stat; see above) against 0 at the group level (second level)
%    Cluster-based random permutation statistics to control for multiple comparisons

stat2 = step4b_sim_comparison_2nd_level(config);

% 3. Extract similarity values from identified clusters (stat2; see above) and contrast between age groups

step4c_sim_comparison_3rd_level(config)


%% Step 5: Plot resulting clusters from step 4 (time-time and topographies)
% required config fields: group, pdat.ga, pdat.stat, pdat.fig, pdat.tfr

step5_plot_clusters(config)


%% Step 6: Plot similarity comparision
% required config fields: pdat.ga, pdat.fig

step6_plot_sim_comparison(config)


%% Step 7: Correlate with memory performance + plot
% required config fields: pdat.ga, pdat.beh, pdat.fig

step7_correlation_with_behavior(config)

