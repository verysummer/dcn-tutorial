function [ sim_matrices ] = betw_cat_rsa( subj, config )
% calculates (global) similarity of time-frequency patterns of stimuli from
% different categories (first presentation of first exemplar)
% --> each stimulus with all other stimuli (pairwise)
%   input:  subj = subject id INT, eg 3101  
%           config = config structure
%   output: time-time simialrity matrices, struct with STIM x CHANNEL x TIME x TIME
%   example: sim = between_cat_rsa(3109,config)

% VRS Mar 2021, adjusted for tutorial Apr 2021

if isfield(config, 'type')
    type = config.type;
else
    error('Missing field in config: need type of RSA information')
end

subj = num2str(subj);

% load data
disp('loading...')
load([config.pdat.tfr '\' subj '_Enc_reduced_tfr_2_125Hz.mat'],'tfr')


%% BOSC

% remove noise spectrum (BOSC)
cfg           = [];
cfg.parameter = 'powspctrm';
cfg.operation = 'log10';

warning('off','stats:statrobustfit:IterationLimit')
disp('running BOSC...')
Data_c = subtract_mean_noise_spectrum(cfg,tfr); % need to be nan free
clear tfr

%% log-transformation

ft_warning off 'FieldTrip:getdimord:warning_dimord_could_not_be_determined'
Data_logpow = ft_math(cfg, Data_c);
Data_logpow.trialinfo2 = Data_c.trialinfo2;
clear Data_c


%% find stimulus pairs

% Between-category similarity (similarity between all first presentations
% of all first exemplars of each category)  

stims = []; % list to save all relevant trials       
for stim = 1:size(Data_logpow.powspctrm,1)          
    % stim is not in pair list yet and it's the first presentation and
    % first exemplar
    if ~ismember(stim,stims) && Data_logpow.trialinfo2{stim,9} == 1 && Data_logpow.trialinfo2{stim,8} == 1
        all_cat{stim} = Data_logpow.trialinfo2{stim,5}; % all categories (not used)
        stims(stim) = stim; 
    end
end
stims(stims==0) = []; % remove zeros 


%% pairwise correlate freq patterns of each item with all other items, for all 60 channels

curr_corrmat = zeros(size(stims,2)-1,size(Data_logpow.powspctrm,2),length(Data_logpow.time),length(Data_logpow.time));
corrmat = zeros(size(stims,2),size(Data_logpow.powspctrm,2),length(Data_logpow.time),length(Data_logpow.time));

fprintf(['Computing pairwise ' type ' similarity. Correlating pairs: ']);
for curr_pair1 = 1:size(stims,2) % all items         
    fprintf('%3d of %3d', curr_pair1, size(stims,2))    
    
    pair = 1;
    for curr_pair2 = 1:size(stims,2) % all other items   
        if curr_pair1 ~= curr_pair2
            for chan = 1:size(Data_logpow.powspctrm,2) % all channels

                tf1 = squeeze(Data_logpow.powspctrm(stims(curr_pair1),chan,:,:)); % tfr of current trial and electrode
                tf2 = squeeze(Data_logpow.powspctrm(stims(curr_pair2),chan,:,:)); 

                % correlate
                curr_corrmat(pair,chan,:,:) = spectral_rsa([],tf1,tf2,Data_logpow.time);  
            end    
            pair = pair + 1;
        end
    end
    % average over pairs to get mean similarity of stim 1 (curr_pair1) with all other stims
    corrmat(curr_pair1,:,:,:) = mean(curr_corrmat,1); 
    curr_corrmat = zeros(size(stims,2)-1,size(Data_logpow.powspctrm,2),length(Data_logpow.time),length(Data_logpow.time)); % back to 0

    fprintf('\b\b\b\b\b\b\b\b\b\b');
end
fprintf(' done \n');


%% make struct, save useful information
sim_matrices = Data_logpow;
sim_matrices.freq = sim_matrices.time; % actually time but called freq
sim_matrices = rmfield(sim_matrices,'powspctrm');
sim_matrices.powspctrm = corrmat; % similarity matrices
sim_matrices.trl = stims'; % used trials (all pairwise correlated) (according to order and not trial number)
sim_matrices.trialinfo2 = Data_logpow.trialinfo2(stims,:); % only used stims/categories
sim_matrices.orig_trialinfo = Data_logpow.trialinfo;
sim_matrices.trialinfo = cell2mat(sim_matrices.trialinfo2(:,1:3)); 
end
    