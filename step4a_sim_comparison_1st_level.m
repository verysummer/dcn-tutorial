function stat = step4a_sim_comparison_1st_level(config)
% First level analysis (within-person): contrast within-item similarity 
% versus between-item (within-category) similarity matrices (paired t-tests)
%   Needs single-subject similarity matrices
%   Saves first level stats
%   Required config fields: subjects, group, pdat.rsa, pdat.stat

% Check configurations
if ~isfield(config, 'subjects')
    error('Missing field in config: need subjects information')
end

if ~isfield(config, 'group')
    error('Missing field in config: need group information')
end

if ~strcmp(config.pdat.rsa(end),filesep)
    warning(['Adding ' filesep ' to end of path config.pdat.rsa'])
    config.pdat.rsa(end+1) = filesep;
end

if ~strcmp(config.pdat.stat(end),filesep)
    warning(['Adding ' filesep ' to end of path config.pdat.stat'])
    config.pdat.stat(end+1) = filesep;
end

if ~isfield(config, 'save')
    config.save = true; % default
end


for subj = 1:length(config.subjects)
    curr_subj = num2str(config.subjects(subj));
    disp(['running subject ' curr_subj])
    
    % load data
    dat1 = load([config.pdat.rsa curr_subj '_within-item_sim_ST.mat']);
    dat2 = load([config.pdat.rsa curr_subj '_within-cat_sim_ST.mat']);

    % paired t-test: need to find pairs of identical categories to test
    % within-item sim against within-cat sim for same categories.
    % find intersecting trials = same categories
    [C,ia,ib] = intersect(cell2mat(dat1.sim_mat.trialinfo2(:,1)), cell2mat(dat2.sim_mat.trialinfo2(:,1)));
    
    % select only those trials existent in both datasets (caution: only
    % powspctrm is changed, not the additional metadata eg pairs info)
    ft_warning off 'FieldTrip:getdimord:warning_dimord_could_not_be_determined'
    cfg = [];
    cfg.trials = ia;
    dat1 = ft_selectdata(cfg, dat1.sim_mat);
    cfg.trials = ib;
    dat2 = ft_selectdata(cfg, dat2.sim_mat);
    
    % number of trials 
    n = size(dat1.powspctrm,1);
    
    %% stat settings
    cfg = [];
    cfg.channel     = 'all';
    cfg.latency     = 'all';
    cfg.frequency   = 'all'; % also time but labeled as freq
    cfg.avgovertime = 'no';
    cfg.avgoverfreq = 'no';
    cfg.avgoverchan = 'no';
    cfg.parameter   = 'powspctrm'; % similarity matrix labeled as TFR
    cfg.method      = 'analytic';
    cfg.tail        = 0; % two sided
    cfg.statistic   = 'ft_statfun_depsamplesT'; 
    
    % design matrix
    cfg.design(1,1:n+n)  = [ones(1,n) 2*ones(1,n)];
    cfg.design(2,1:n+n)  = [1:n 1:n]; 
    cfg.ivar             = 1; % the 1st row in cfg.design contains the independent variable (condition)
    cfg.uvar             = 2; % the 2nd row in cfg.design contains the trial number
    
    % run
    stats{subj} = ft_freqstatistics(cfg, dat1, dat2);  
    dfs(subj) = stats{subj}.df; % save dfs
end

% GA over all subjects
cfg = [];
cfg.keepindividual = 'yes';
cfg.parameter = 'stat';
stat = ft_freqgrandaverage(cfg,stats{:});
stat.df = dfs;
    
if config.save
    save([config.pdat.stat config.group '_within-item_vs_within-cat_1st_level_stat.mat'],'stat','-v7.3')
else
    warning('Not saving stats because config.save = false')
end

end

