function stat2 = step4b_sim_comparison_2nd_level(config)
% Second level analysis (within-group): contrast t-maps of first level
% (within-item similarity versus within-category similarity) against zero
%   Needs first level stats
%   Saves second level stats
%   Required config fields: subjects, group, pdat.tfr, pdat.stat

% Check configurations
if ~isfield(config, 'subjects')
    error('Missing field in config: need subjects information')
end

if ~isfield(config, 'group')
    error('Missing field in config: need group information')
end

if ~strcmp(config.pdat.tfr(end),filesep)
    warning(['Adding ' filesep ' to end of path config.pdat.tfr'])
    config.pdat.tfr(end+1) = filesep;
end

if ~strcmp(config.pdat.stat(end),filesep)
    warning(['Adding ' filesep ' to end of path config.pdat.stat'])
    config.pdat.stat(end+1) = filesep;
end

if ~isfield(config, 'save')
    config.save = true; % default
end

%%
% load first level stats
load([config.pdat.stat config.group '_within-item_vs_within-cat_1st_level_stat.mat'], 'stat')

% load elec
cfg = load([config.pdat.tfr 'elec']);
cfg.elec.elecpos = cfg.elec.pnt;
cfg.elec.chanpos = cfg.elec.pnt;
cfg.layout = ft_prepare_layout(cfg);  

% contrast to 0
null = stat;
null.stat = zeros(size(stat.stat));

%% settings for stat
cfg.channel          = 'all'; 
cfg.latency          = 'all';
cfg.frequency        = 'all';  % also time but labeled as freq
cfg.avgoverchan      = 'no';
cfg.avgovertime      = 'no'; 
cfg.avgoverfreq      = 'no';
cfg.method           = 'montecarlo';
cfg.statistic        = 'indepsamplesT'; % depsamplesT
cfg.correctm         = 'cluster'; 
cfg.clusteralpha     = 0.05; 
cfg.clusterstatistic = 'maxsum';
cfg.minnbchan        = 1; % min 1 electrode in one cluster
cfg.tail             = 0; % two sided
cfg.clustertail      = 0; 
cfg.alpha            = 0.025;    
cfg.numrandomization = 500;      
cfg.parameter        = 'stat'; 

% specifies with which sensors other sensors can form clusters
cfg_neighb = [];
cfg_neighb.method  = 'distance';
ft_warning off 'FieldTrip:getdimord:warning_dimord_could_not_be_determined'
cfg.neighbours = ft_prepare_neighbours(cfg_neighb, cfg.elec);   

% design matrix
subj = size(stat.stat,1);
if strcmp(cfg.statistic, 'depsamplesT')
    design = zeros(2,subj*2);
    for i = 1:subj
        design(1,i) = i; 
    end 
    for  i = 1:subj
        design(1,subj+i) = i; 
    end 
    design(2,1:subj) = 1; 
    design(2,subj+1:2*subj)= 2; 
    cfg.uvar = 1; 
    cfg.ivar = 2; 
    
elseif strcmp(cfg.statistic, 'indepsamplesT')
    design = zeros(1,subj*2);
    design(1,1:subj) = 1; 
    design(1,subj+1:2*subj)= 2; 
    cfg.ivar = 1; 
end

cfg.design = design; 

%% run 
stat2 = ft_freqstatistics(cfg, stat, null);

if config.save
    save([config.pdat.stat config.group '_within-item_vs_within-cat_2nd_level_cluster_stat.mat'],'stat2','-v7.3')
else
    warning('Not saving stats because config.save = false')
end


