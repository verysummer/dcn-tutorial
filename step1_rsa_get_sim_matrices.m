function step1_rsa_get_sim_matrices(config)
% Loops through all subjects and calculates specified similarity matrices
%   Needs single-subject TFRs
%   Saves single-subject similarity matrices 
%   Required config fields: subjects, type, pdat.tfr, pdat.rsa
% 
%   VRS Feb 2019, adjusted for tutorial Apr 2021

% Check configurations
if isfield(config, 'subjects')
    subjects = config.subjects;
else
    error('Missing field in config: need subjects information')
end

if isfield(config, 'type')
    type = config.type;
else
    error('Missing field in config: need type of RSA information')
end

if ~strcmp(config.pdat.tfr(end),filesep)
    warning(['Adding ' filesep ' to end of path config.pdat.tfr'])
    config.pdat.tfr(end+1) = filesep;
end

if ~strcmp(config.pdat.rsa(end),filesep)
    warning(['Adding ' filesep ' to end of path config.pdat.rsa'])
    config.pdat.rsa(end+1) = filesep;
end

if ~isfield(config, 'save')
    config.save = true; % default
end

%% RSA for each subject
for s = 1:length(subjects)
    tic
    subj = subjects(s);
    fprintf('running subject %d (%d of %d) \n', subj, s, length(subjects))

    % self-sim / within-category sim
    if ~strcmp(type,'between-cat')
       % get similarity matrices
        sim_mat = self_rsa(subj, config);
        % between-category (global) similarity    
    elseif strcmp(type,'between-cat')
        % get similarity matrices
        sim_mat = betw_cat_rsa(subj, config);
    end
    
% if you want subject-specific folders:
%     if ~exist([config.pdat.rsa num2str(subj)],'dir')
%         mkdir([config.pdat.rsa num2str(subj)])
%     end
    
    % save similarity data
    if config.save
        fprintf('saving %d (%d of %d) \n', subj, s, length(subjects))
        save([config.pdat.rsa num2str(subj) '_' type '_sim_ST.mat'],'sim_mat','-v7.3')
    else
        warning('Not saving single-subject similarity data because config.save = false')
    end
    clear sim_mat
    toc
end

disp('All done')
end
