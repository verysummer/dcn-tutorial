function [ sim_matrices ] = self_rsa( subj, config )
% calculates self-similarity of time-frequency patterns of identical
% stimuli at different presentations
% OR within-category similarity (first presentations)
%   input:  subj = subject id INT, eg 3101
%           config = config structure
%   output: time-time similarity matrices, struct with STIM x CHANNEL x TIME x TIME
%   example: sim_1_2 = self_rsa(3109, config)

% VRS Feb 2019, adjusted for tutorial Apr 2021

if isfield(config, 'type')
    type = config.type;
else
    error('Missing field in config: need type of RSA information')
end

subj = num2str(subj);

% load data
disp('loading...')
load([config.pdat.tfr '\' subj '_Enc_reduced_tfr_2_125Hz.mat'],'tfr')

% compare presentations 1 and 2 
pres1 = 1;
pres2 = 2;
only_first_ex = 1; % only first exemplar in within-item similarity


%% BOSC

% remove noise spectrum (BOSC)
cfg           = [];
cfg.parameter = 'powspctrm';
cfg.operation = 'log10';

warning('off','stats:statrobustfit:IterationLimit')
disp('running BOSC...')
Data_c = subtract_mean_noise_spectrum(cfg,tfr); % need to be nan free
clear tfr

%% log-transformation

ft_warning off 'FieldTrip:getdimord:warning_dimord_could_not_be_determined'
Data_logpow = ft_math(cfg, Data_c);
Data_logpow.trialinfo2 = Data_c.trialinfo2;
clear Data_c

%% find stimulus pairs

% find first and second (or other; change above) presentations of identical stimuli 
if strcmp(type,'within-item')
    
    pairs = []; % list to save all pairs       
    for stim1 = 1:size(Data_logpow.powspctrm,1)-1           
        % stim is not in pair list yet and it's the first presentations
        if ~ismember(stim1,pairs) && Data_logpow.trialinfo2{stim1,9} == pres1 
            
            % if only want first exemplar 
            if only_first_ex
                if Data_logpow.trialinfo2{stim1,8} == 1 % first exemplar
                    curr_pair = Data_logpow.trialinfo2{stim1,7}; % current stim item
                    pairs(stim1,1) = stim1; 
                end
            else % all exemplars
                curr_pair = Data_logpow.trialinfo2{stim1,7}; % current stim item
                pairs(stim1,1) = stim1; 
            end
            
            for stim2 = stim1+1:size(Data_logpow.powspctrm,1)
                % it's the current pair and its second presentation
                if strcmp(Data_logpow.trialinfo2{stim2,7},curr_pair) && Data_logpow.trialinfo2{stim2,9} == pres2 
                    
                    % if only want first exemplar 
                    if only_first_ex
                        if Data_logpow.trialinfo2{stim2,8} == 1
                            pairs(stim1,2) = stim2;
                        end
                    else % all exemplars
                        pairs(stim1,2) = stim2;
                    end
                end
            end
        end
    end
    pairs(~all(pairs,2),:) = []; % remove zeros (no partner)
    
    
% Within-category similarity
elseif strcmp(type,'within-cat')
    
    pairs = []; % list to save all pairs       
    for stim1 = 1:size(Data_logpow.powspctrm,1)-1           
        % stim is not in pair list yet and it's the first presentations
        if ~ismember(stim1,pairs) && Data_logpow.trialinfo2{stim1,9} == 1
            
            curr_pair = Data_logpow.trialinfo2{stim1,5}; % current category
            pairs(stim1,1) = stim1; 
                     
            for stim2 = stim1+1:size(Data_logpow.powspctrm,1)
                % it's the current pair and its second exemplar, first presentation
                if strcmp(Data_logpow.trialinfo2{stim2,5},curr_pair) && Data_logpow.trialinfo2{stim2,8} == 2 && Data_logpow.trialinfo2{stim2,9} == 1                    
                    pairs(stim1,2) = stim2; 
                end
            end
        end
    end
    pairs(~all(pairs,2),:) = []; % remove zeros (no partner)
end


%% correlate the two freq patterns of each pair, for all channels

% pairs x channel x time x time
corrmat = zeros(size(pairs,1),size(Data_logpow.powspctrm,2),length(Data_logpow.time),length(Data_logpow.time));

fprintf(['Computing ' type ' similarity. Correlating pair: ']);
for curr_pair = 1:size(pairs,1) % all item pairs         
    fprintf('%3d of %3d', curr_pair, size(pairs,1))    
    
    for chan = 1:size(Data_logpow.powspctrm,2) % all channels
        
        tf1 = squeeze(Data_logpow.powspctrm(pairs(curr_pair,1),chan,:,:)); % tfr of current trial and electrode
        tf2 = squeeze(Data_logpow.powspctrm(pairs(curr_pair,2),chan,:,:)); 
        
        % correlate
        corrmat(curr_pair,chan,:,:) = spectral_rsa([],tf1,tf2,Data_logpow.time);
    end    
    fprintf('\b\b\b\b\b\b\b\b\b\b');
end
fprintf(' done \n');

%% make struct, save useful information
sim_matrices = Data_logpow;
sim_matrices.orig_trialinfo = Data_logpow.trialinfo; 
sim_matrices.freq = sim_matrices.time; % actually time but called freq
sim_matrices = rmfield(sim_matrices,'powspctrm');
sim_matrices.powspctrm = corrmat; % similarity matrices
sim_matrices.pairs = pairs; % item pair information
sim_matrices.trialinfo2 = Data_logpow.trialinfo2(pairs(:,1),:); % only used stims/categories
sim_matrices.trialinfo = cell2mat(sim_matrices.trialinfo2(:,1:3)); 
sim_matrices.indtrials = Data_logpow.trialinfo2(sort([pairs(:,1);pairs(:,2)]),:); % individual trials used for RSA
end

