# Tutorial for spectral EEG pattern similarity analysis

See the methodological tutorial paper for how to use the data analysis pipeline: https://psyarxiv.com/87hpb/

Sample dataset can be found on: https://osf.io/jbrsa/

Cite as: 
Sommer, V. R., Mount, L., Weigelt, S., Werkle-Bergner, M., & Sander, M. C. (2021). Spectral Pattern Similarity Analysis: Tutorial and Application in Developmental Cognitive Neuroscience. https://doi.org/10.31234/osf.io/87hpb
