function step6_plot_sim_comparison(config)
% Plot within-item sim vs within-cat sim
% (connected scatter, raincloud)
%   Needs individual mean similarities
%   Saves plots comparing similarities and age groups
%   Required config fields: pdat.ga, pdat.fig

% Check configurations
if ~strcmp(config.pdat.ga(end),filesep)
    warning(['Adding ' filesep ' to end of path config.pdat.ga'])
    config.pdat.ga(end+1) = filesep;
end

if ~strcmp(config.pdat.fig(end),filesep)
    warning(['Adding ' filesep ' to end of path config.pdat.fig'])
    config.pdat.fig(end+1) = filesep;
end

if ~isfield(config, 'save')
    config.save = true; % default
end

%%
load([config.pdat.ga 'CH+YA_mean_within-item_sim_in_cluster.mat'],'wi_sim')
load([config.pdat.ga 'CH+YA_mean_within-cat_sim_in_cluster.mat'],'wc_sim')
load([config.pdat.ga 'CH+YA_mean_item_specificity_in_cluster.mat'],'ispec')

%% similarity comparison

f=figure('Position', [711 526 364 420]);
movegui(f,'center');
hold on
for g = {'CH','YA'}
    group = cell2mat(g);   
    if strcmp(group, 'CH')
        col = [0 159/244 227/244];
        mark = 'x';
    elseif strcmp(group, 'YA')
        col = 'k';
        mark = 'o';
    end
    scatter(ones(length(wi_sim.(group)),1), wi_sim.(group), 'MarkerEdgeColor', col, 'LineWidth', 1, 'Marker', mark)
    scatter(2*ones(length(wi_sim.(group)),1), wc_sim.(group), 'MarkerEdgeColor', col, 'LineWidth', 1, 'Marker', mark)
    x = [1 2];
    for i = 1:length(wi_sim.(group))  
        y = [wi_sim.(group)(i) wc_sim.(group)(i)];
        plot(x, y, 'Color', col, 'LineWidth', 1);
    end
    ax=gca;
    ax.XLim = [0.7,2.3];
    ax.YLim = [0.2 0.63];
    ax.XTick = [1 2];
    ax.XTickLabel = {'Within-item', 'Between-item'};
    ax.FontSize = 14;
    ylabel('Pattern similarity')
end

if config.save
    export_fig([config.pdat.fig 'CH+YA_mean_sim_in_cluster'], '-png','-r300','-transparent')  
else
    warning('Not saving figure because config.save = false')
end


%% group comparison

bw = 0.006; % CH

f=figure('Position',[565 679 739 326]);
movegui(f,'center');
hold on
h1 = raincloud_plot('X',  ispec.YA, 'box_on', 1,'bandwidth', bw, 'color', [0 0 0],'alpha', 0.5, 'box_dodge', 1, 'box_dodge_amount', .35, 'dot_dodge_amount', .15, 'box_col_match', 1);
h1 = raincloud_plot('X', ispec.CH, 'box_on', 1,'bandwidth', bw, 'color', [0 159/244 227/244], 'alpha', 0.5, 'box_dodge', 1, 'box_dodge_amount', .55, 'dot_dodge_amount', .15, 'box_col_match', 1);
ax = gca;
%ax.XLim = XLim;
ax.YLim = [-35 40];
ax.YTick =[];
ax.FontSize = 13;
set(ax,'ycolor','none')
xlabel('Item specificity')

% save figure 
if config.save
    export_fig([config.pdat.fig 'CH+YA_item_spec_in_clusters_raincloud'], '-png', '-r300','-transparent') 
else
    warning('Not saving figure because config.save = false')
end

end
