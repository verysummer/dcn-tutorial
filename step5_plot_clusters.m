function step5_plot_clusters(config)
% Plots clusters: 1. time-time stats (t values), 2. topoplot (t values)
% 3. similarity with cluster outlines (time-time)
%   Needs first and second level stats, GA similarity matrices
%   Saves cluster plots
%   Required config fields: group, pdat.ga, pdat.stat, pdat.fig, pdat.tfr

% Check config
if isfield(config, 'group')
    group = config.group;
else
    error('Missing field in config: need group information')
end

if ~strcmp(config.pdat.ga(end),filesep)
    warning(['Adding ' filesep ' to end of path config.pdat.ga'])
    config.pdat.ga(end+1) = filesep;
end

if ~strcmp(config.pdat.stat(end),filesep)
    warning(['Adding ' filesep ' to end of path config.pdat.stat'])
    config.pdat.stat(end+1) = filesep;
end

if ~strcmp(config.pdat.fig(end),filesep)
    warning(['Adding ' filesep ' to end of path config.pdat.fig'])
    config.pdat.fig(end+1) = filesep;
end

if ~strcmp(config.pdat.tfr(end),filesep)
    warning(['Adding ' filesep ' to end of path config.pdat.tfr'])
    config.pdat.tfr(end+1) = filesep;
end

if ~isfield(config, 'save')
    config.save = true; % default
end

%% find clusters and create mask

% load similarity data (GA), first level stats, second level stats
disp('loading data...')
load([config.pdat.ga group '_within-item_sim_GA.mat'], 'sim')
wi = sim;
load([config.pdat.ga group '_within-cat_sim_GA.mat'], 'sim')
wc = sim; clear sim
load([config.pdat.stat group '_within-item_vs_within-cat_1st_level_stat.mat'], 'stat')
load([config.pdat.stat group '_within-item_vs_within-cat_2nd_level_cluster_stat.mat'], 'stat2') 

% set level of significance
p_crit = stat2.cfg.alpha;

% find relevant clusters
if ~isempty(stat2.posclusters)
    pos_cluster_pvals = [stat2.posclusters(:).prob];
    pos_signif_clust = find(pos_cluster_pvals < p_crit);
    num_pos_clust = length(pos_signif_clust);
    disp([num2str(num_pos_clust) ' positive cluster(s) with p < ' num2str(p_crit)])
else
    pos_signif_clust = [];
end
if ~isempty(stat2.negclusters)
    neg_cluster_pvals = [stat2.negclusters(:).prob];
    neg_signif_clust = find(neg_cluster_pvals < p_crit);
    num_neg_clust = length(neg_signif_clust);
    disp([num2str(num_neg_clust) ' negative cluster(s) with p < ' num2str(p_crit)])
else
    neg_signif_clust = [];
end

% create mask
mask = NaN(size(stat2.negclusterslabelmat));
for sign_clust = 1:length(pos_signif_clust)
    mask((find(stat2.posclusterslabelmat == sign_clust))) = 1; % all sign positive clusters
end
for sign_clust = 1:length(neg_signif_clust)
    mask((find(stat2.negclusterslabelmat == sign_clust))) = 1; % all sign negative clusters
end 
mask_mean = nanmean(mask,1); % average over electrodes
mask_mean(isnan(mask_mean)) = 0;
mask_mean = logical(mask_mean);

% average across channels
% stat
cfg = [];
cfg.avgoverchan = 'yes';
cfg.nanmean = 'yes';
ft_warning off 'FieldTrip:getdimord:warning_dimord_could_not_be_determined'
stat_mean = ft_selectdata(cfg,stat2);
stat_mean.mask = mask_mean;
% wi similarity
cfg.avgoverrpt = 'yes';
wi_mean = ft_selectdata(cfg,wi);
wi_mean.mask = mask_mean;
% wc similarity
wc_mean = ft_selectdata(cfg,wc);
wc_mean.mask = mask_mean;


%% figures

% 1. stat

% create colormap
Cm = cbrewer('div', 'RdBu', 128, 'PCHIP'); % diverging! 'RdBu'
Cm = flipud(Cm); % puts red on top, blue at the bottom

cfg                = [];
cfg.maskstyle      = 'opacity';  %'outline'; %'opacity' 'saturation';
cfg.maskalpha      = 0; % if opacity
cfg.colormap       = Cm;
cfg.parameter      = 'stat';
cfg.maskparameter  = 'mask' ;
cfg.zlim           = 'maxabs';  % symmetrical color scale 

% plot 
f=figure('Position',[491 505 630 473]);
movegui(f,'center');
cfg.figure = 'gcf';
ft_singleplotTFR(cfg,stat_mean); 
title('')
axis square

% configure axes:
ax = gca; 
ax.Box = 'off';
ax.XAxis.LineWidth = 1.5;
ax.YAxis.LineWidth = 1.5;
ax.FontSize = 16;
ax.XLabel.String = 'trial time';
ax.YLabel.String = 'trial time';

% further settings for figure
fig = gcf;
fig.Color = [1 1 1];
c = colorbar;
c.Label.String = 'effect size (t)';
%c.Label.Rotation = 0';
c.Label.VerticalAlignment = 'middle';
c.Location = 'eastoutside';
%c.FontSize = 16; 
c.Label.LineWidth = 30; 

% save figure 
if config.save
    export_fig([config.pdat.fig num2str(size(stat.stat,1)) group '_t_in_clusters'], '-png', '-r300','-transparent') 
else
    warning('Not saving figure because config.save = false')
end

%% topoplot

load([config.pdat.tfr 'elec.mat'])

% prepare layout
cfg = [];
cfg.elec = elec;
cfg.elec.pnt(:,1) = -elec.pnt(:,2);
cfg.elec.pnt(:,2) = elec.pnt(:,1);

cfg.highlight = 'on';
cfg.highlightchannel = stat2.label(~isnan(squeeze(nanmean(nanmean(mask,2),3)))); 
cfg.highlightsize = 15;
cfg.colormap = Cm;
cfg.parameter = 'stat';
cfg.comment = 'no';
cfg.zlim = 'maxabs';  % symmetrical color scale 

f=figure;
movegui(f,'center');
cfg.figure = 'gcf';
ft_topoplotER(cfg,stat2);
colorbar
c = colorbar;
c.Label.String = 'effect size (t)';
ax = gca; 
ax.FontSize = 16;

% save figure
if config.save
    export_fig([config.pdat.fig num2str(size(stat.stat,1)) group '_topo_t_in_clusters'], '-png', '-r300','-transparent') 
else
    warning('Not saving figure because config.save = false')
end

%% 2. similarities

mini = round(min(min([wi_mean.powspctrm wc_mean.powspctrm])),2);
maxi = round(max(max([wi_mean.powspctrm wc_mean.powspctrm])),2);

% create colormap
Cm = cbrewer('seq', 'YlOrRd', 64, 'PCHIP'); % sequential!

cfg                = [];
cfg.maskstyle      = 'outline';  %'outline'; %'opacity' 'saturation';%
cfg.colormap       = Cm;
cfg.parameter      = 'powspctrm';
cfg.maskparameter  = 'mask' ;
cfg.zlim           = [mini maxi];  % symmetrical color scale 

% plot 
f=figure('Position',[124 435 1451 570]);
movegui(f,'center');
cfg.figure = 'gcf';
for f = 1:2
    if f == 1
        dat = wi_mean;
        t = 'within-item';
    else
        dat = wc_mean;
        t = 'within-category';
    end
    
    subplot(1,2,f)
    ft_singleplotTFR(cfg,dat); 
    title(t)
    axis square

    % configure axes:
    ax = gca; 
    ax.Box = 'off';
    ax.XAxis.LineWidth = 1.5;
    ax.YAxis.LineWidth = 1.5;
    ax.FontSize = 16;
    ax.XLabel.String = 'trial time';
    ax.YLabel.String = 'trial time';

    % further settings for figure
    fig = gcf;
    fig.Color = [1 1 1];
    c = colorbar;
    c.Label.String = 'similarity (z'')';
    %c.Label.Rotation = 0';
    c.Label.VerticalAlignment = 'middle';
    c.Location = 'eastoutside';
    %c.FontSize = 16; 
    c.Label.LineWidth = 30; 
    set(gca, 'Layer', 'top');
    grid off
end

% save figure 
if config.save
    export_fig([config.pdat.fig num2str(size(stat.stat,1)) group '_similarity_in_clusters'], '-png', '-r300','-transparent') 
else
    warning('Not saving figure because config.save = false')
end

end