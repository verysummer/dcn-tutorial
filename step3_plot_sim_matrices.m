function step3_plot_sim_matrices(config)
% Plots specified averaged time-time similarity matrices
%   if type is not specified, will plot both within-item and within-cat on
%   one figure (same scale)
%   Needs grand average similarity matrices
%   Saves group average similarity plot, diagonal
%   Required config fields: group, pdat.ga, pdat.fig 
% 
%   VRS Feb 2019, adjusted for tutorial Apr 2021

% Check configurations
if isfield(config, 'type')
    type = {config.type};
else
    type = {'within-item', 'within-cat'};
end

if isfield(config, 'group')
    group = config.group;
else
    error('Missing field in config: need group information')
end

if ~strcmp(config.pdat.ga(end),filesep)
    warning(['Adding ' filesep ' to end of path config.pdat.ga'])
    config.pdat.ga(end+1) = filesep;
end

if ~strcmp(config.pdat.fig(end),filesep)
    warning(['Adding ' filesep ' to end of path config.pdat.fig'])
    config.pdat.fig(end+1) = filesep;
end

if ~isfield(config, 'save')
    config.save = true; % default
end

% load specified type or both WI and WC
for t = 1:length(type)        
    disp('loading data...')
    load([config.pdat.ga group '_' type{t} '_sim_GA.mat'], 'sim')

    % average similarity matrices and select time points
    cfg = [];
    cfg.nanmean = 'yes';
    cfg.avgoverchan = 'yes';
    cfg.avgoverrpt = 'yes';
    cfg.latency = [-0.6 2];
    cfg.frequency = [-0.6 2]; % (also time)
    ft_warning off 'FieldTrip:getdimord:warning_dimord_could_not_be_determined'
    dat{t} = ft_selectdata(cfg, sim);
    mean_dat{t} = squeeze(dat{t}.powspctrm); % all data
end

%% figure

% get min and max for plotting
mini = round(min(min([mean_dat{:}])),2);
maxi = round(max(max([mean_dat{:}])),2);

subjects = dat{1}.subj;

% plot
% create colormap
Cm = cbrewer('seq', 'YlOrRd', 64, 'PCHIP'); % sequential!

cfg                = [];
cfg.colormap       = Cm;
cfg.parameter      = 'powspctrm';
cfg.zlim           = [mini maxi];  % symmetrical color scale 

if numel(dat) > 1
    f=figure('Position',[124 435 1451 570]);
else
    f=figure('Position',[472 95 754 570]);
end
cfg.figure = 'gcf';
movegui(f,'center');
for t = 1:numel(dat)
    subplot(1,numel(dat),t)
    ft_singleplotTFR(cfg,dat{t}); 
    colormap(Cm)
    axis square

    % configure axes:
    ax = gca; 
    ax.Box = 'off';
    ax.XAxis.LineWidth = 1.5;
    ax.YAxis.LineWidth = 1.5;
    ax.FontSize = 16;
    ax.XLabel.String = 'trial time';
    ax.YLabel.String = 'trial time';

    % further settings for figure
    fig = gcf;
    fig.Color = [1 1 1];
    c = colorbar;
    c.Label.String = 'similarity (z'')';
    %c.Label.Rotation = 0';
    c.Label.VerticalAlignment = 'middle';
    c.Location = 'eastoutside';
    %c.FontSize = 16; 
    c.Label.LineWidth = 30; 
    set(gca, 'Layer', 'top');
    grid off
    
    if strcmp(type{t}, 'within-item')
        title(['within-item similarity ' num2str(length(subjects)) ' ' group])
    elseif strcmp(type{t}, 'within-cat')
        title(['within-category similarity ' num2str(length(subjects)) ' ' group])
    elseif strcmp(type{t}, 'between-cat')
        title(['between-category similarity ' num2str(length(subjects)) ' ' group])
    end
end
% save figure
if config.save
    if length(type) == 1
        export_fig([config.pdat.fig num2str(length(subjects)) group '_' type{1} '_sim'], '-png','-r300','-transparent')  
    else
        export_fig([config.pdat.fig num2str(length(subjects)) group '_' type{1} '+' type{2} '_sim'], '-png','-r300','-transparent')  
    end
else
    warning('Not saving figure because config.save = false')
end


% plot diagonals
if strcmp(group, 'CH')
    col = [0 159/244 227/244];
elseif strcmp(group, 'YA')
    col = 'k';
end

f=figure('Position',[440 333 795 420]);
movegui(f,'center');
hold on
for t = 1:numel(dat)
    if t == 1
        lin = '-';
    else
        lin = ':';
    end
    mean_dat{t} = diag(mean_dat{t});
    plot(sim.time,mean_dat{t}, 'Color', col, 'LineWidth', 1.5, 'LineStyle', lin)
end
% configure axes:
ax = gca; 
ax.Box = 'off';
ax.XAxis.LineWidth = 1.5;
ax.YAxis.LineWidth = 1.5;
ax.FontSize = 16;
ax.XLabel.String = 'trial time';
ax.YLabel.String = 'pattern similarity';
ax.XLim = [-0.6 2];
legend({'within-item','within-category'},'Location','southwest')
title('Diagonal')

% save figure
if config.save
    if length(type) == 1
        export_fig([config.pdat.fig num2str(length(subjects)) group '_' type{1} '_sim_diagonal'], '-png','-r300','-transparent')  
    else
        export_fig([config.pdat.fig num2str(length(subjects)) group '_' type{1} '+' type{2} '_sim_diagonal'], '-png','-r300','-transparent')  
    end
else
    warning('Not saving figure because config.save = false')
end

end
