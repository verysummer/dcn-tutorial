function step2_rsa_group_ga(config)
% Make grand average of pattern similarity data
%   First average over trials for each subject, then GA of all subjects;
%   keeps individuals
%   Needs single-subject similarity matrices
%   Saves grand average similarity matrices
%   Required config fields: group, subjects, type, pdat.rsa, pdat.ga
% 
%   VRS Feb 2019, adjusted for tutorial Apr 2021

% Check configurations
if isfield(config, 'subjects')
    subjects = config.subjects;
else
    error('Missing field in config: need subjects information')
end

if isfield(config, 'group')
    group = config.group;
else
    error('Missing field in config: need group information')
end

if isfield(config, 'type')
    type = config.type;
else
    error('Missing field in config: need type of RSA information')
end

if ~strcmp(config.pdat.rsa(end),filesep)
    warning(['Adding ' filesep ' to end of path config.pdat.rsa'])
    config.pdat.rsa(end+1) = filesep;
end

if ~strcmp(config.pdat.ga(end),filesep)
    warning(['Adding ' filesep ' to end of path config.pdat.ga'])
    config.pdat.ga(end+1) = filesep;
end

if ~isfield(config, 'save')
    config.save = true; % default
end

fprintf('Computing grand average of %d subjects \n', length(subjects))
tic
for subj = 1:length(subjects)
    curr_subj = num2str(subjects(subj));
    disp(['loading ' curr_subj '...'])

    load([config.pdat.rsa curr_subj '_' type '_sim_ST.mat'],'sim_mat')
    
    ft_warning off 'FieldTrip:getdimord:warning_dimord_could_not_be_determined'
    % average trials of each subject
    if ~isempty(sim_mat.powspctrm)
        cfg = [];
        cfg.avgoverrpt = 'yes';
        avg_sim_mat{subj} = ft_selectdata(cfg, sim_mat);  
        included_subj{subj} = curr_subj;
    end
end

avg_sim_mat = avg_sim_mat(~cellfun('isempty',avg_sim_mat));

% GA over all subjects
cfg = [];
cfg.keepindividual = 'yes';
sim = ft_freqgrandaverage(cfg,avg_sim_mat{:});
sim.subj = included_subj;

if config.save
    fprintf('Saving grand average of %d subjects... \n', subj)
    save([config.pdat.ga group '_' type '_sim_GA'], 'sim', '-v7.3')
else
    warning('Not saving grand average because config.save = false')
end

clear sim avg_sim_mat
toc
end
