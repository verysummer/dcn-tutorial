function step4c_sim_comparison_3rd_level(config)
% Extract similarity values from identified clusters and
% contrast between age groups
%   Needs first and second level stats, single-subject similarity matrices
%   Saves third level stats, individual mean similarities
%   Required config fields: pdat.stat, pdat.ga

% Check configurations
if ~strcmp(config.pdat.ga(end),filesep)
    warning(['Adding ' filesep ' to end of path config.pdat.ga'])
    config.pdat.ga(end+1) = filesep;
end

if ~strcmp(config.pdat.stat(end),filesep)
    warning(['Adding ' filesep ' to end of path config.pdat.stat'])
    config.pdat.stat(end+1) = filesep;
end

if ~isfield(config, 'save')
    config.save = true; % default
end

%% load and extract similarity from clusters
for groups = {'CH', 'YA'}
    group = cell2mat(groups);

    disp('loading...')
    % load similarity data (GA), first level stats, second level stats
    load([config.pdat.ga group '_within-item_sim_GA.mat'], 'sim')
    wi = sim;
    load([config.pdat.ga group '_within-cat_sim_GA.mat'], 'sim')
    wc = sim; clear sim
    load([config.pdat.stat group '_within-item_vs_within-cat_1st_level_stat.mat'], 'stat')
    load([config.pdat.stat group '_within-item_vs_within-cat_2nd_level_cluster_stat.mat'], 'stat2')
  
    % set level of significance 
    p_crit = stat2.cfg.alpha;

    % find relevant clusters
    if ~isempty(stat2.posclusters)
        pos_cluster_pvals = [stat2.posclusters(:).prob];
        pos_signif_clust = find(pos_cluster_pvals < p_crit);
        num_pos_clust = length(pos_signif_clust);
        disp([num2str(num_pos_clust) ' positive cluster(s) with p < ' num2str(p_crit)])
    else
        pos_signif_clust = [];
    end
    if ~isempty(stat2.negclusters)
        neg_cluster_pvals = [stat2.negclusters(:).prob];
        neg_signif_clust = find(neg_cluster_pvals < p_crit);
        num_neg_clust = length(neg_signif_clust);
        disp([num2str(num_neg_clust) ' negative cluster(s) with p < ' num2str(p_crit)])
    else
        neg_signif_clust = [];
    end

    % create mask
    mask = NaN(size(stat2.negclusterslabelmat));
    for sign_clust = 1:length(pos_signif_clust)
        mask((find(stat2.posclusterslabelmat == sign_clust))) = 1; % all sign positive clusters
    end
    for sign_clust = 1:length(neg_signif_clust)
        mask((find(stat2.negclusterslabelmat == sign_clust))) = 1; % all sign negative clusters
    end

    % for each subject get similarity within clusters
    for subj = 1:size(wi.powspctrm,1)
        wi.powspctrm2(subj,:,:,:) = squeeze(wi.powspctrm(subj,:,:,:)) .* mask;
        wc.powspctrm2(subj,:,:,:) = squeeze(wc.powspctrm(subj,:,:,:)) .* mask;
    end
        
    % compute item specificty (wi minus wc)
    cfg = [];
    cfg.parameter = 'powspctrm2';
    cfg.operation = 'subtract';
    ft_warning off 'FieldTrip:getdimord:warning_dimord_could_not_be_determined'
    ispec.(group) = ft_math(cfg, wi, wc);
    
    % settings to average 
    cfg = [];
    cfg.avgoverchan = 'yes';
    cfg.avgovertime = 'yes';
    cfg.avgoverfreq = 'yes';
    cfg.nanmean = 'yes';
    
    % average similarities
    wi_means = ft_selectdata(cfg, wi);
    wi_sim.(group) = wi_means.powspctrm2;
    wc_means = ft_selectdata(cfg, wc);
    wc_sim.(group) = wc_means.powspctrm2;
    
    % average item specificity
    ispec_means = ft_selectdata(cfg, ispec.(group));
    ispec.(group) = ispec_means.powspctrm2;    
end

%% test

[H,P,CI,STATS] = ttest2(ispec.CH, ispec.YA);
if H
    fprintf('Found significant differences in item specificity between children and adults: t = %3.2f, p = %1.3f\n' , STATS.tstat, P)
else
    fprintf('Found no significant differences in item specificity between children and adults: t = %3.2f, p = %1.3f\n' , STATS.tstat, P)
end

% save
if config.save
    disp('Saving stats and individual mean similarity data')
    save([config.pdat.stat 'CH_vs_YA_item_spec_in_cluster_3rd_level'],'H','P','CI','STATS')
    save([config.pdat.ga 'CH+YA_mean_within-item_sim_in_cluster.mat'],'wi_sim')
    save([config.pdat.ga 'CH+YA_mean_within-cat_sim_in_cluster.mat'],'wc_sim')
    save([config.pdat.ga 'CH+YA_mean_item_specificity_in_cluster.mat'],'ispec')
else
    warning('Not saving because config.save = false')
end

end
