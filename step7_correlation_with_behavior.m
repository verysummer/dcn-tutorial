function step7_correlation_with_behavior(config)
% Between-subject association of item specificty and memory performance
%   Needs individual mean similarities, individual item memory
%   Saves scatter plot
%   Required config fields: pdat.ga, pdat.beh, pdat.fig

% Check configurations
if ~strcmp(config.pdat.ga(end),filesep)
    warning(['Adding ' filesep ' to end of path config.pdat.ga'])
    config.pdat.ga(end+1) = filesep;
end

if ~strcmp(config.pdat.beh(end),filesep)
    warning(['Adding ' filesep ' to end of path config.pdat.beh'])
    config.pdat.beh(end+1) = filesep;
end

if ~strcmp(config.pdat.fig(end),filesep)
    warning(['Adding ' filesep ' to end of path config.pdat.fig'])
    config.pdat.fig(end+1) = filesep;
end

if ~isfield(config, 'save')
    config.save = true; % default
end

%%
load([config.pdat.beh 'CH+YA_mean_item_memory'],'item_mem')
load([config.pdat.ga 'CH+YA_mean_item_specificity_in_cluster.mat'],'ispec')

[r,p] = corr(ispec.CH,item_mem.CH);
if p < 0.05
    fprintf('Found significant correlation of item specificity and item memory in children: r = %3.2f, p = %1.3f\n' , r, p)
else
    fprintf('Found no significant correlation of item specificity and item memory in children: r = %3.2f, p = %1.3f\n' , r, p)
end
[r,p] = corr(ispec.YA,item_mem.YA);
if p < 0.05
    fprintf('Found significant correlation of item specificity and item memory in adults: r = %3.2f, p = %1.3f\n' , r, p)
else
    fprintf('Found no significant correlation of item specificity and item memory in adults: r = %3.2f, p = %1.3f\n' , r, p)
end
[r,p] = corr([ispec.CH;ispec.YA], [item_mem.CH;item_mem.YA]);
if p < 0.05
    fprintf('Found significant correlation of item specificity and item memory across groups: r = %3.2f, p = %1.3f\n' , r, p)
else
    fprintf('Found no significant correlation of item specificity and item memory across groups: r = %3.2f, p = %1.3f\n' , r, p)
end

figure
hold on
scatter(ispec.YA,item_mem.YA,'MarkerEdgeColor', 'k', 'LineWidth', 1.5, 'Marker', 'o')
scatter(ispec.CH,item_mem.CH,'MarkerEdgeColor', [0 159/244 227/244], 'LineWidth', 1.5, 'Marker', 'x')
h = lsline;
set(h(1),'color',[0 159/244 227/244])
set(h(2),'color','k')
ax=gca;
ax.FontSize = 14;
ylabel('item memory')
xlabel('item specificity')

% save figure 
if config.save
    export_fig([config.pdat.fig 'CH+YA_corr_item_spec_mem'], '-png', '-r300','-transparent') 
else
    warning('Not saving figure because config.save = false')
end

end
